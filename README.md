Interview Question: Behavior Flow Graph

Background: What is a Behavior Flow Graph?
A behavior flowgraph is a tool used to identify areas of significant dropoff in a product experience, so they can be addressed. It is also used to understand the major paths people are taking through the app. An example for a signup wall with a register button and login button may look like the example below.

It is similar to a funnel. However, in a funnel you might combine multiple actions into a single step. In a behavior flow graph, you instead let it branch out at each decision point and end up constructing a tree rather than a linear funnel. This gives a more accurate representation of user behavior than a funnel.

A Real World Example:
|---register_button (10)
|    |---register_email (4)
|    |    |---email_already_exists (1)
|    |    |---register_success (3)
|    |---register_facebook (4)
|    |    |---register_success (4)
|    |---dropoff (2)
|---login_button (10)
|    |---login_email (4)
|    |    |---login_success (4)
|    |---login_facebook (4)
|    |    |---login_success (3)
|    |    |---login_failure (1)
|    |---dropoff (2)

Simple Example
Input:
user_id, timestamp, action
100, 1000, A
200, 1003, A
300, 1009, B
100, 1026, B
100, 1030, C
200, 1109, B
200, 1503, A




Tree from the Input:
|---A (2)
|    |---B (2)
|    |    |---C (1)
|    |    |---A (1)
|---B (1)


Questions
1) First, can you define the data structure to represent a node in the behavior flow graph?
2) Can you write a function (in any languague of your choice) that given input, constructs the behavior flow graph. Code should be production quality.
3) Can you write a function given the root of the behavior flow graph, it prints out the same ascii representation used in the example (and also remember to print dropoff counts)
4) Now imagine you had to construct this graph from terabytes of logs. How would you do that?



