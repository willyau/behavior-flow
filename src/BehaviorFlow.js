// @flow
/*
 * Assumptions:
 * 1. The end of the actions (`dropoff`) is a normal action so we don't need
 *    to provide this construct in the implementation
 * 2. New and stopping condition for a flow are all unclear from the example.
 *    The example also showed that `A` was repeated so in this implemention we
 *    continue to insert from the users' last action
 * 3. The example shows all events are in-order. So in this implementation, we assume
 *    that all events coming in are in order. If events are out of order, extra
 *    care must be taken to ensure proper insertion and maintainance.
 */
const ROOT_NODE = 'root';

class Event {
  action: string;
  userId: number;
  timestamp: number;
}

/**
 * Question #1
 */
class Node {
  // Tracks the action type
  action: string;

  // The children nodes to represent the flow mapped by action as the key.
  // Using a map allows us to get the action node with O(1) but also ensure
  // that there's only one node for each action.
  childrenNodes: Map<string, Node>;

  // NOTE: due to our current assumptions, we do not intent on resetting and
  // establishing a new flow. However, if this requirement is needed, we can use
  // the provided Event timestamp and store it along with the user id in
  // the node. This allows comparing the new event with the last node and see if
  // the new event should be considered the start of a new flow.
  userIds: Set<number>;

  constructor(action: string, userId?: number) {
    this.action = action;
    this.childrenNodes = new Map();
    this.userIds = new Set();

    if (userId) {
      this.userIds.add(userId);
    }
  }

  addOrUpdateChildActionNode(userId: number, action: string) {
    let childNode = this.childrenNodes.get(action);
    if (childNode) {
      if (!childNode.userIds.has(userId)) {
        childNode.userIds.add(userId);
      }
      // NOTE: Assumption is that there are no repeats.
      // Otherwise, maybe timestamp check or maybe it's a duplicate.
      // More work is needed is the condition differ from the assumption.
    } else {
      childNode = new Node(action, userId);
      this.childrenNodes.set(action, childNode);
    }

    return childNode;
  }
}

export default class BehaviorFlow {
  // The root node for the Behavior Flow Graph
  graph: Node;

  // NOTE: If this is persisted somewhere (maybe with a node id), it can be allow
  // better scaling as the graph grows bigger.
  // This property allows insertion without walking down the tree to find the last node
  lastUserNodes: Map<number, Node>;

  constructor() {
    this.graph = new Node(ROOT_NODE);
    this.lastUserNodes = new Map();
  }

  /**
   * Question #2
   * @param {Event[]} events
   */
  addEvents(events: Event[]) {
    events.forEach(event => this.addEvent(event));
  }

  addEvent({ action, userId }: Event) {
    const lastUserNode = this.lastUserNodes.get(userId);
    let newUserNode = null;

    if (lastUserNode) {
      // NOTE: we could store timestamp in the node for this user and check
      // with the current event's timestamp. If the current event's timestamp
      // is out of order, we'll need to update the exist flow for this user
      // Because we've assumed that the series of events are in order, we
      // don't need to specifically check for the timestamp.
      newUserNode = lastUserNode.addOrUpdateChildActionNode(userId, action);
    } else {
      newUserNode = this.graph.addOrUpdateChildActionNode(userId, action);
    }

    this.lastUserNodes.set(userId, newUserNode);
  }

  /**
   * Question #3
   */
  printAscii(printer: (msg: string) => void) {
    this.doPrintAscii(printer, this.graph.childrenNodes, 0);
  }

  /**
   * DFS
   */
  doPrintAscii(printer: (msg: string) => void, childrenNodes: Map<string, Node>, depth: number) {
    if (childrenNodes.size === 0) {
      return;
    }

    const prefix = '|    ';
    childrenNodes.forEach(node => {
      printer(
        `${prefix.repeat(depth)}|---${node.action} (${node.userIds.size})\n`
      );
      this.doPrintAscii(printer, node.childrenNodes, depth + 1);
    });
  }
}

/**
 Question 4
 We can partition the input data by user. This allows us to incrementally build
 out the behavior tree. As the tree receives more data per user, it becomes more
 full. It would also allow segmenting the graph per user when viewing.

 For example, each user's flow can be kept chronological by partitioning the input data.
 We can then use mapreduce to build a flow tree for each user. Then another
 mapreduce to merge trees of different users.

 Instead of mapreduce, if there's a realtime requirement, we may want to look into
 using realtime solutions such as Spark.

 Another point of interest is knowing when / how to reset
 the flow. With a sufficient amount of time delta between the previous user action,
 we may want to start a new flow so the user action is not continuously growing
 (like how it is with our current assumption/implementation).
 */
