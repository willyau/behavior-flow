const BehaviorFlow = require('./BehaviorFlow').default;

describe('BehaviorFlow', () => {
  const bf = new BehaviorFlow();
  // Events are ordered
  const inputs = [
    { userId: 100, timestamp: 1000, action: 'A' },
    { userId: 200, timestamp: 1003, action: 'A' },
    { userId: 300, timestamp: 1009, action: 'B' },
    { userId: 100, timestamp: 1026, action: 'B' },
    { userId: 100, timestamp: 1030, action: 'C' },
    { userId: 200, timestamp: 1109, action: 'B' },
    { userId: 200, timestamp: 1503, action: 'A' }
  ];

  bf.addEvents(inputs);

  describe('data structure', () => {
    test('should have 2 root childrenNodes', () => {
      expect(bf.graph.childrenNodes.size).toBe(2);
      expect(bf.graph.childrenNodes.get('A').action).toBe('A');
      expect(bf.graph.childrenNodes.get('B').action).toBe('B');
    });

    describe('first node (A)', () => {
      const nodeA = bf.graph.childrenNodes.get('A');
      const nodeB = nodeA.childrenNodes.get('B');
      test('should have 1 node (B)', () => {
        expect(nodeA.childrenNodes.size).toBe(1);
        expect(nodeB.action).toBe('B');
      });

      test('should have 2 users', () => expect(nodeA.userIds.size).toBe(2));

      describe('node (B)', () => {
        test('should have 2 childrenNodes (C, A)', () => {
          expect(nodeB.childrenNodes.size).toBe(2);
          expect(nodeB.childrenNodes.get('C').action).toBe('C');
          expect(nodeB.childrenNodes.get('A').action).toBe('A');
        });

        describe('node (C)', () => {
          test('should have 1 user', () =>
            expect(nodeB.childrenNodes.get('C').userIds.size).toBe(1));
        });

        describe('node (A)', () => {
          test('should have 1 user', () =>
            expect(nodeB.childrenNodes.get('A').userIds.size).toBe(1));
        });

        test('should have 2 users', () => expect(nodeB.userIds.size).toBe(2));
      });
    });

    describe('2nd node (B)', () => {
      const nodeB = bf.graph.childrenNodes.get('B');
      test('should have 0 childrenNodes', () => {
        expect(nodeB.childrenNodes.size).toBe(0);
      });

      test('should have 1 user', () => expect(nodeB.userIds.size).toBe(1));
    });
  });

  describe('printAscii', () => {
    test('should print as expected', () => {
      let result = '';
      const printer = msg => {
        result += msg;
      };

      bf.printAscii(printer);
      expect(result).toBe(
`|---A (2)
|    |---B (2)
|    |    |---C (1)
|    |    |---A (1)
|---B (1)
`
      );
    });
  });
});
